# Exercise

Hello, thank your for applying at CATIE.

Before we get to the interviews we would like to know a little bit more about
your skills with some real-world use cases. The goal of theses
exercices is overview your coding and problem solving skills.

Before getting to the point. Please mind that:

- You should not spend more than 4 hours on each exercices.
- Partial results are accepted.

These subjects are close to real-world problem we face at CATIE. 

The second subject is called “Survey the users”, it focuses on Web Application programming. We currently build 
a web application and we use web technologies in most of our projects for its ease of distribution and the 
large programmer communities.

Both subject can either be trivial or quite rich and deep once you start digging. Depending on your experience 
work on the one you feel the most competent with and write some ideas on how to solve the other one.
Please time your work. Do not spend too much time on these subjects.

You can send any questions to:  j.laviole@catie.fr and your results as a gitlab/github project, a zip/tar file 
to the same email address.

## Survey the users

In this exercice, you need to build a survey to find out if a user likes a product or not.
The survey has to be a web application that can store the results in a database.

#### Administration

It should be easy to add new questions, and the app have to handle at least two types of
answers: text and and 1 to 5 scale.
For this assignment we suppose that is an administrator that is a power user (knows the application
well) or a programmer. Easy is defined as: just a few lines of code, or less than a minute
in an interface.

Bonus: All the results can be collected in a single CSV file, from the application or a conversion service.

#### User inputs

The user has to be able to answer questions on a 1 to 5 scale and answer text
on other questions. The 1 to five represents the negative - neutral - positive answer.

Once all the answers are submitted a thank you message appears and invite the user to close the page.

Example of HTML surveys:
* https://cdn.smassets.net/assets/cms/cc/uploads/cs-survey-screenshot.png
* https://static-cms.hotjar.com/images/survey-example.width-750.png
* https://www.miqols.org/cqol/wp-content/uploads/2019/03/example-survey-02.png

### Technical requirements

The application must be a web application, the server and database have to be able to run
either on linux or windows. You can use pre-existing webservices, components or frameworks.

Constraints: 
- You do not need to add authentication or user management.
- The application must have some javascript, using either vanilla (standard) - ES6 - coffescript - React or VueJS.
- Frontend must be in HTML using a framework like Bootstrap, SemanticUI, Materialize etc...
- Use technologies that you know, or want to discover.
- You can use any number of JS libraries, and backend admin interfaces.

If you want to have a rich backend, please use one of these languages:

- Ruby: Rails / Sinatra, or other.
- Python: Django / Flask, or other.
- NodeJS.
- Php.

### Expected results

This is a problem solving task, use your knowledge and tools to build a solution.
The time constraint shoud be a strong limit in this exercise. An online deployment would be appreciated.

